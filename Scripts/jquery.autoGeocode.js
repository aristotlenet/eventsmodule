﻿/*
Automatically geocode the address in a form with address, city, state, and zip fields, and place the result in latitude and longitude fields.
* Can optionally set the status of the geocode in a given status element, which offers further geocode controls.
* By default will override current value of Lat/Lng fields if minAccuracy is met, otherwise leaving them unchanged. Can optionally only geocode if the fields are empty.
 
Example usage:
autoGeocode({
    latitudeField: "[id$=latitudeTextBox]", //required; this field will be set after successful geocode
    longitudeField: "[id$=longitudeTextBox]", //required; this field will be set after successful geocode
    countyField: "[id$=countyTextBox]", //optional; this field will be set after successful geocode
    addressField: "[id$=addressTextBox]",
    cityField: "[id$=cityTextBox]",
    stateField: "[id$=stateSelector]",
    zipField: "[id$=zipTextBox]",
    defaultCity: "Little Rock", //either cityField or defaultCity required
    defaultState: "AR", //either stateField or defaultState required
    defaultZip: "72201", //either zipField or defaultZip required
    onlyIfEmpty: false, //optional; defaults to false
    minAccuracy: 'intersection', //optional; defaults to 'intersection'; uses Google API address components; see accuracyMap in source for values
    statusField: ".geocodeStatus" //optional; uses console if not set
});
*/
 
// prevent errors when window.console doesn't exist. Set up console shims before including this file.
if (!window.console) window.console = { log: jQuery.noop, debug: jQuery.noop, warn: jQuery.noop };
 
(function($){
window.autoGeocode = function(options) {
    var instance = this;
    var settings = {
        onlyIfEmpty: false,
        minAccuracy: 'intersection'
    };
    $.extend(true, settings, options);

    if (typeof google == 'undefined' || typeof google.maps == 'undefined') {
        console.log("Google maps not loaded");
        return;
    }
 
    if (!settings.hasOwnProperty('latitudeField')) { console.error('latitudeField required'); return; }
    if (!settings.hasOwnProperty('longitudeField')) { console.error('longitudeField required'); return; }
    if (!settings.hasOwnProperty('addressField')) { console.error('addressField required'); return; }
    if (!settings.hasOwnProperty('cityField') && !settings.hasOwnProperty('defaultCity')) { console.error('cityField or defaultCity required'); return; }
    if (!settings.hasOwnProperty('stateField') && !settings.hasOwnProperty('defaultState')) { console.error('stateField or defaultState required'); return; }
    if (!settings.hasOwnProperty('zipField') && !settings.hasOwnProperty('defaultZip')) { console.error('zipField or defaultZip required'); return; }
    var latitudeField = $(settings.latitudeField);
    var longitudeField = $(settings.longitudeField);
    var countyField = $(settings.countyField);
    var addressField = $(settings.addressField);
    var cityField = $(settings.cityField);
    var stateField = $(settings.stateField);
    var zipField = $(settings.zipField);
    var statusField = $(settings.statusField);
 
    var accuracyMap = {
        unknown_accuracy: 0
        ,post_box: 2200 //indicates a specific postal box.
        ,room: 2100 //indicates the room of a building address.
        ,floor: 2000 //indicates the floor of a building address.
        ,street_number: 1900 //indicates the precise street number.
        ,subpremise: 1800 //indicates a first-order entity below a named location, usually a singular building within a collection of buildings with a common name
        ,premise: 1700 //indicates a named location, usually a building or collection of buildings with a common name
        ,street_address: 1600 //indicates a precise street address.
        ,natural_feature: 1500 //indicates a prominent natural feature.
        ,establishment: 1450 //indicates a business (hard to find info on this - "business" is an educated guess)
        ,park: 1400 //indicates a named park.
        ,point_of_interest: 1350 //indicates a named point of interest. Typically, these "POI"s are prominent local entities that don't easily fit in another category such as "Empire State Building" or "Statue of Liberty."
        ,airport: 1300 //indicates an airport.
        ,intersection: 1200 //indicates a major intersection, usually of two major roads.
        ,neighborhood: 1100 //indicates a named neighborhood
        ,route: 1000 //indicates a named route (such as "US 101").
        ,postal_code: 900 //indicates a postal code as used to address postal mail within the country.
        ,sublocality: 800 //indicates an first-order civil entity below a locality
        ,locality: 700 //indicates an incorporated city or town political entity.
        ,political: 600 //indicates a political entity. Usually, this type indicates a polygon of some civil administration.
        ,administrative_area_level_3: 500 //indicates a third-order civil entity below the country level. This type indicates a minor civil division. Not all nations exhibit these administrative levels.
        ,administrative_area_level_2: 400 //indicates a second-order civil entity below the country level. Within the United States, these administrative levels are counties. Not all nations exhibit these administrative levels.
        ,administrative_area_level_1: 300 //indicates a first-order civil entity below the country level. Within the United States, these administrative levels are states. Not all nations exhibit these administrative levels.
        //,colloquial_area: 1000 //indicates a commonly-used alternative name for the entity.
        ,country: 100 //indicates the national political entity, and is typically the highest order type returned by the Geocoder.
    };

    var geocoder = new google.maps.Geocoder();
 
 
    var getAccuracyValue = function(accuracyText) {
        return accuracyMap.hasOwnProperty(accuracyText) ? accuracyMap[accuracyText] : null; 
    };
 
    var getAccuracyFromResults = function(results) {
        return results[0].address_components[0].types[0];
    };
 
    var meetsAccuracy = function(results, minAccuracyValue) {
        var accuracyText = getAccuracyFromResults(results);
        var accuracyValue = getAccuracyValue(accuracyText);
        return !minAccuracyValue || accuracyValue >= minAccuracyValue;
    };
 
    var getCountyFromResults = function(results) {
        var countyComponents = $.grep(results[0].address_components, function(r) { 
            return $.inArray("administrative_area_level_2", r.types) >= 0; 
        });
        if (countyComponents.length > 0)
            return countyComponents[0].long_name;
        return '';
    };
 
    var setStatus = function(wasSuccessful, statusHtml, statusText) {
        if (statusField.length > 0) {
            var statusFieldContent = $('<div class="autoGeocodeStatus"><h3></h3><div></div></div>');
            if (wasSuccessful) {
                statusFieldContent
                    .addClass('success')
                    .find('h3')
                        .text('OK')
                        .end()
                    .find('div')
                        .append(statusHtml);
                ;
            } else {
                statusFieldContent
                    .addClass('error') 
                    .find('h3')
                        .text('Failed')
                        .end()
                    .find('div')
                        .append(statusHtml);
                ;
            }
            statusField
                .animate(
                    {opacity:0},
                    {
                        duration:100,
                        complete: function(){
                            statusField
                                .empty()
                                .append(statusFieldContent)
                                .animate({opacity:1}, {duration:100})
                                ;
                        }
                    }
                );
        } else {
            console.log('Geocode success: ' + wasSuccessful + '; Status: ' + statusText);
        }
    };
 
    this.codeAddress = function () {
        var addressArray = [
            addressField.val(),
            (cityField.val() || settings.defaultCity || ""),
            (stateField.val() || settings.defaultState || ""),
            (zipField.val() || settings.defaultZip || "")
            ];
 
        //Stop if any fields are empty
        if ($.inArray("", addressArray) > -1) { return; }
 
        if (settings.onlyIfEmpty
            && (latitudeField.val() != "" || longitudeField.val() != "")) {
            return;
        }
 
        var address = addressArray.join(",");
 
        geocoder.geocode(
            { 'address': address },
            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var accuracy = getAccuracyFromResults(results);
                    var location = results[0].geometry.location;
                    if (meetsAccuracy(results, minAccuracyValue)) {
                        latitudeField.val(location.lat());
                        longitudeField.val(location.lng());
                        countyField.val(getCountyFromResults(results));
                        setStatus(true, 'Accuracy: <strong>' + accuracy + '</strong>', 'Accuracy: ' + accuracy);
                    } else {
                        var html = $('<span>Insufficient accuracy (' + accuracy + ')<button>Use Anyway</button></span>');
                        html.find('button').click(function(e){
                            latitudeField.val(location.lat());
                            longitudeField.val(location.lng());
                            e.preventDefault();
                        });
                        setStatus(false, html, 'Insufficient accuracy (' + accuracy + ')');
                    }
                } else {
                    var text = "An error occurred: " + status;
                    setStatus(false, text, text);
                }
            }
        );
    };
 
    //Initialization
    var minAccuracyValue = getAccuracyValue(settings.minAccuracy);
 
    $.each([addressField, cityField, stateField, zipField], function() {
        this.change(codeAddress);
    });
 
    //run on ready for editing a previously ungeocoded location, even if they don't change the address
    $(function () { codeAddress(); });
};
})(jQuery);