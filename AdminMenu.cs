﻿
using Orchard.Localization;
using Orchard.UI.Navigation;


namespace Contrib.Events {
    public class AdminMenu : INavigationProvider {
        public AdminMenu() {
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        public string MenuName {
            get { return "admin"; }
        }

        public void GetNavigation(NavigationBuilder builder) {
            builder
                .Add(T("Events"), "1", LinkSubMenu);
        }

        private void LinkSubMenu(NavigationBuilder menu) {
            menu.Add(item => item
                .Position("1")
                .Caption(T("Events"))
                .Action("List", "EventAdmin", new { area = "Contrib.Events" })
                .Permission(EventPermissions.MetaListOwnEvents));
        }
    }
}
