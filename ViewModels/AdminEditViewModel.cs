﻿using System.ComponentModel.DataAnnotations;
using Orchard.Core.Common.ViewModels;

namespace Contrib.Events.ViewModels {
    public class AdminEditViewModel {
        [Required]
        public virtual DateTimeEditor StartDateEditor { get; set; }
        public virtual DateTimeEditor EndDateEditor { get; set; }

        public virtual string Phone { get; set; }
        public virtual string Email { get; set; }
        public virtual string Website { get; set; }

        public virtual string Address { get; set; }
        public virtual string AdditionalAddressInfo { get; set; }
        [Required]
        public virtual string City { get; set; }
        [Required]
        public virtual string State { get; set; }
        [Required]
        public virtual string ZipCode { get; set; }

        public virtual string GpsLat { get; set; }
        public virtual string GpsLng { get; set; }

        public virtual string ContactName { get; set; }
        public virtual string ContactEmail { get; set; }
        public virtual string ContactPhone { get; set; }
    }
}