﻿using System;
using Orchard.ContentManagement;

namespace Contrib.Events.ViewModels {
    public class EventAdminContentOptions {
        public EventAdminContentOptions() {
            BulkAction = EventsBulkAction.None;
            OrderBy = EventsOrder.StartDate;
            Filter = EventsStatus.Active;
        }

        public EventsBulkAction BulkAction { get; set; }
        public EventsOrder OrderBy { get; set; }
        public EventsStatus Filter { get; set; }
    }

    public class EventEntry {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public ContentItem ContentItem { get; set; }
        public bool IsChecked { get; set; }
    }

    public enum EventsBulkAction {
        None,
        Publish,
        Unpublish,
        Delete
    }

    public enum EventsOrder {
        StartDate,
        EndDate,
        Created,
        Published,
        Modified
    }

    public enum EventsStatus {
        Active,
        Archived,
        Pending,
        All
    }
}