﻿using Contrib.Events.Models;
using Orchard.ContentManagement.Handlers;
using Orchard.Data;


namespace Contrib.Events.Handlers {
    public class EventPartHandler : ContentHandler {
        public EventPartHandler(
            IRepository<EventPartRecord> repository ) {
            Filters.Add(StorageFilter.For(repository));

            OnIndexing<EventPart>((context, eventPart) => context.DocumentIndex
                .Add("event_startDate", eventPart.StartDate.Value.Date).Analyze().Store()
                .Add("event_endDate", (eventPart.EndDate.HasValue ? eventPart.EndDate.Value : eventPart.StartDate).Value.Date).Analyze().Store()
                .Add("event_startTime", eventPart.StartDate.Value).Analyze().Store()
                .Add("event_endTime", (eventPart.EndDate.HasValue ? eventPart.EndDate.Value : eventPart.StartDate).Value.Date).Analyze().Store());
        }
    }
}