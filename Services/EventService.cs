﻿using System.Collections.Generic;
using System.Linq;
using Contrib.Events.Models;
using JetBrains.Annotations;
using Orchard.ContentManagement;
using Orchard.Core.Common.Models;

namespace Contrib.Events.Services
{
    [UsedImplicitly]
    public class EventService : IEventService {
        private readonly IContentManager _contentManager;

        public EventService(IContentManager contentManager) {
            _contentManager = contentManager;
        }

        public ContentItem Get(int id, VersionOptions versionOptions = null) {
            if (versionOptions == null) {
                versionOptions = VersionOptions.AllVersions;
            }
            var eventPart = _contentManager.Get<EventPart>(id, versionOptions);
            return eventPart == null ? null : eventPart.ContentItem;
        }

        public IEnumerable<EventPart> Get() {
            return Get(VersionOptions.Published);
        }

        public IEnumerable<EventPart> Get(VersionOptions versionOptions) {
            return GetEventQuery(versionOptions)
                .List();
        }

        public IEnumerable<EventPart> Get(int skip, int count) {
            return Get(skip, count, VersionOptions.Published);
        }

        public IEnumerable<EventPart> Get(int skip, int count, VersionOptions versionOptions) {
            return GetEventQuery(versionOptions)
                    .Slice(skip, count)
                    .ToList();
        }

        public void Delete(ContentItem @event) {
            _contentManager.Remove(@event);
        }

        private IContentQuery<EventPart> GetEventQuery(VersionOptions versionOptions) {
            return _contentManager.Query<EventPart, EventPartRecord>(versionOptions)
                .Join<CommonPartRecord>().OrderByDescending(cr => cr.CreatedUtc);
        }
    }
}