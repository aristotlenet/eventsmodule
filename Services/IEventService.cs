﻿using System.Collections.Generic;
using Contrib.Events.Models;
using Orchard;
using Orchard.ContentManagement;

namespace Contrib.Events.Services {
    public interface IEventService : IDependency {
        ContentItem Get(int id, VersionOptions versionOptions = null);
        IEnumerable<EventPart> Get();
        IEnumerable<EventPart> Get(VersionOptions versionOptions);
        IEnumerable<EventPart> Get(int skip, int count);
        IEnumerable<EventPart> Get(int skip, int count, VersionOptions versionOptions);
        void Delete(ContentItem @event);
    }
}