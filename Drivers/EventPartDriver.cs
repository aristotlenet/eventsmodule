﻿using System;
using System.Globalization;
using System.Xml;
using Contrib.Events.Models;
using Contrib.Events.ViewModels;
using Orchard;
using Orchard.ArchiveLater.Models;
using Orchard.ArchiveLater.Services;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Core.Common.ViewModels;
using Orchard.Localization.Services;


namespace Contrib.Events.Drivers {
    public class EventPartDriver : ContentPartDriver<EventPart> {
        private readonly IDateServices _dateServices;
        private readonly IArchiveLaterService _archiveLaterService;

        protected override string Prefix {
            get { return "Contrib.Events.EventPart"; }
        }


        public EventPartDriver(
            IOrchardServices services,
            IDateServices dateServices,
            IArchiveLaterService archiveLaterService) {
            _dateServices = dateServices;
            _archiveLaterService = archiveLaterService;
            Services = services;
        }

        public IOrchardServices Services { get; set; }

        protected override DriverResult Display(EventPart part, string displayType, dynamic shapeHelper) {
            return Combined(
                ContentShape("Parts_Event",
                    () => shapeHelper.Parts_EventPart()),
                ContentShape("Parts_Event_Summary",
                    () => shapeHelper.Parts_Event_Summary()),
                ContentShape("Parts_Event_Detail",
                    () => shapeHelper.Parts_Event_Detail()),
                ContentShape("Parts_Event_Metadata_SummaryAdmin",
                    () => shapeHelper.Parts_Event_Metadata_SummaryAdmin())
            );
        }

        protected override DriverResult Editor(EventPart part, dynamic shapeHelper) {
            var startDate = part.StartDate.HasValue ? part.StartDate.Value : new DateTime();
            var endDate = part.EndDate.HasValue ? part.EndDate.Value : startDate;

            var model = new AdminEditViewModel {
                StartDateEditor = new DateTimeEditor {
                    ShowDate = true,
                    ShowTime = true,
                    Date = _dateServices.ConvertToLocalDateString(startDate),
                    Time = _dateServices.ConvertToLocalTimeString(startDate),
                },
                EndDateEditor = new DateTimeEditor {
                    ShowDate = true,
                    ShowTime = true,
                    Date = _dateServices.ConvertToLocalDateString(endDate),
                    Time = _dateServices.ConvertToLocalTimeString(endDate),
                },
                Phone = part.Phone,
                Email = part.Email,
                Website = part.Website,
                Address = part.Address,
                AdditionalAddressInfo = part.AdditionalAddressInfo,
                City = part.City,
                State = part.State,
                ZipCode = part.ZipCode,
                GpsLat = part.GpsLat,
                GpsLng = part.GpsLng,
                ContactName = part.ContactName,
                ContactEmail = part.ContactEmail,
                ContactPhone = part.ContactPhone,
            };
            return ContentShape("Parts_Event_Edit",
                () => shapeHelper.EditorTemplate(
                    TemplateName: "Parts.EventPart",
                    Model: model,
                    Prefix: Prefix));
        }

        protected override DriverResult Editor(EventPart part, IUpdateModel updater, dynamic shapeHelper) {
            var model = new AdminEditViewModel();
            var dateTime = DateTime.MinValue;

            updater.TryUpdateModel(model, Prefix, null, null);

            if (String.IsNullOrWhiteSpace(model.EndDateEditor.Date)) {
                model.EndDateEditor.Date = model.StartDateEditor.Date;
            }

            var startDate = _dateServices.ConvertFromLocalString(model.StartDateEditor.Date, model.StartDateEditor.Time ?? dateTime.TimeOfDay.ToString());

            if (startDate.HasValue)
                part.StartDate = startDate;

            var endDate = _dateServices.ConvertFromLocalString(model.EndDateEditor.Date, model.EndDateEditor.Time ?? dateTime.TimeOfDay.ToString());

            part.EndDate = endDate;
            _archiveLaterService.ArchiveLater(part.ContentItem, endDate.Value);

            part.Phone = model.Phone;
            part.Email = model.Email;
            part.Website = model.Website;

            part.Address = model.Address;
            part.AdditionalAddressInfo = model.AdditionalAddressInfo;
            part.City = model.City;
            part.State = model.State;
            part.ZipCode = model.ZipCode;

            part.GpsLat = model.GpsLat;
            part.GpsLng = model.GpsLng;

            part.ContactName = model.ContactName;
            part.ContactEmail = model.ContactEmail;
            part.ContactPhone = model.ContactPhone;

            return Editor(part, shapeHelper);
        }

        protected override void Importing(EventPart part, ImportContentContext context)
        {
            var partName = part.PartDefinition.Name;

            var startDate = context.Attribute(part.PartDefinition.Name, "CreatedUtc");
            if (startDate != null) {
                context.ImportAttribute(partName, "StartDate", value => part.StartDate = XmlConvert.ToDateTime(startDate, XmlDateTimeSerializationMode.Utc));
            }


            var endDate = context.Attribute(part.PartDefinition.Name, "CreatedUtc");
            if (endDate != null) {
                context.ImportAttribute(partName, "EndDate", value => part.EndDate = XmlConvert.ToDateTime(endDate, XmlDateTimeSerializationMode.Utc));
            }

            context.ImportAttribute(partName, "Phone", value => part.Phone = value);
            context.ImportAttribute(partName, "Email", value => part.Email = value);
            context.ImportAttribute(partName, "Website", value => part.Website = value);
            context.ImportAttribute(partName, "Address", value => part.Address = value);
            context.ImportAttribute(partName, "AdditionalAddressInfo", value => part.AdditionalAddressInfo = value);
            context.ImportAttribute(partName, "City", value => part.City = value);
            context.ImportAttribute(partName, "State", value => part.State = value);
            context.ImportAttribute(partName, "ZipCode", value => part.ZipCode = value);
            context.ImportAttribute(partName, "GpsLat", value => part.GpsLat = value);
            context.ImportAttribute(partName, "GpsLng", value => part.GpsLng = value);
            context.ImportAttribute(partName, "ContactName", value => part.ContactName = value);
            context.ImportAttribute(partName, "ContactEmail", value => part.ContactEmail = value);
            context.ImportAttribute(partName, "ContactPhone", value => part.ContactPhone = value);
        }

        protected override void Exporting(EventPart part, ExportContentContext context) {
            var element = context.Element(part.PartDefinition.Name);

            if (part.StartDate != null) {
                element.SetAttributeValue("StartDate", XmlConvert.ToString(part.StartDate.Value, XmlDateTimeSerializationMode.Utc));
            }

            if (part.EndDate != null) {
                element.SetAttributeValue("EndDate", XmlConvert.ToString(part.EndDate.Value, XmlDateTimeSerializationMode.Utc));
            }

            element.SetAttributeValue("Phone", part.Phone);
            element.SetAttributeValue("Email", part.Email);
            element.SetAttributeValue("Website", part.Website);
            element.SetAttributeValue("Address", part.Address);
            element.SetAttributeValue("AdditionalAddressInfo", part.AdditionalAddressInfo);
            element.SetAttributeValue("City", part.City);
            element.SetAttributeValue("State", part.State);
            element.SetAttributeValue("ZipCode", part.ZipCode);
            element.SetAttributeValue("GpsLat", part.GpsLat);
            element.SetAttributeValue("GpsLng", part.GpsLng);
            element.SetAttributeValue("ContactName", part.ContactName);
            element.SetAttributeValue("ContactEmail", part.ContactEmail);
            element.SetAttributeValue("ContactPhone", part.ContactPhone);
        }

        protected override void Imported(EventPart part, ImportContentContext context) {
            var archiveLaterPart = part.As<ArchiveLaterPart>();

            if (part.EndDate.HasValue) {
                if (!archiveLaterPart.ScheduledArchiveUtc.Value.HasValue) {
                    _archiveLaterService.ArchiveLater(part.ContentItem, part.EndDate.Value);
                }
            }
        }
    }
}