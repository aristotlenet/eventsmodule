﻿using Contrib.Events.Models;
using Orchard.ContentManagement.Drivers;
using Orchard.Environment.Extensions;


namespace Contrib.Events.Drivers {
    [OrchardFeature("Contrib.Events.Search")]
    public class EventSearchPartDriver : ContentPartDriver<EventSearchPart> {
        protected override string Prefix {
            get { return "Contrib.Events.EventSearchPart"; }
        }

        protected override DriverResult Display(EventSearchPart part, string displayType, dynamic shapeHelper) {
            return ContentShape("Parts_EventSearch",
                () => shapeHelper.Parts_EventSearch());
        }
    }
}