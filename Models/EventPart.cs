﻿using System;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Aspects;
using Orchard.Core.Common.Models;
using Orchard.Core.Title.Models;

namespace Contrib.Events.Models {
    public class EventPart : ContentPart<EventPartRecord> {
        public string Title {
            get { return this.As<TitlePart>().Title; }
            set { this.As<TitlePart>().Title = value; }
        }

        public string Description {
            get { return this.As<BodyPart>().Text; }
            set { this.As<BodyPart>().Text = value; }
        }

        public DateTime? StartDate {
            get { return Record.StartDate; }
            set { Record.StartDate = value; }
        }

        public DateTime? EndDate {
            get { return Record.EndDate; }
            set { Record.EndDate = value; }
        }

        public string Phone {
            get { return Record.Phone; }
            set { Record.Phone = value; }
        }

        public string Email {
            get { return Record.Email; }
            set { Record.Email = value; }
        }

        public string Website {
            get { return Record.Website; }
            set { Record.Website = value; }
        }

        public string Address {
            get { return Record.Address; }
            set { Record.Address = value; }
        }

        public string AdditionalAddressInfo {
            get { return Record.AdditionalAddressInfo; }
            set { Record.AdditionalAddressInfo = value; }
        }

        public string City {
            get { return Record.City; }
            set { Record.City = value; }
        }

        public string State {
            get { return Record.State; }
            set { Record.State = value; }
        }

        public string ZipCode {
            get { return Record.ZipCode; }
            set { Record.ZipCode = value; }
        }

        public string GpsLat {
            get { return Record.GpsLat; }
            set { Record.GpsLat = value; }
        }

        public string GpsLng {
            get { return Record.GpsLng; }
            set { Record.GpsLng = value; }
        }

        public string ContactName {
            get { return Record.ContactName; }
            set { Record.ContactName = value; }
        }

        public string ContactEmail {
            get { return Record.ContactEmail; }
            set { Record.ContactEmail = value; }
        }

        public string ContactPhone {
            get { return Record.ContactPhone; }
            set { Record.ContactPhone = value; }
        }

        public DateTime? PublishedDate {
            get { return this.As<ICommonPart>().PublishedUtc; }
        }

        public bool HasCoordinates {
            get {
                return !(string.IsNullOrEmpty(GpsLat) || GpsLat == "0"
                         || string.IsNullOrEmpty(GpsLng) || GpsLng == "0");
            }
        }

        public string CityStateZip {
            get { return string.Format("{0}, {1} {2}", City, "AR", ZipCode).Trim(' ', ','); }
        }

        public string GeocodeAddress {
            get { return string.Format("{0}, {1}, {2} {3}", Address, City, State, ZipCode); }
        }
    }
}