﻿using System;
using Orchard.ContentManagement.Records;

namespace Contrib.Events.Models {
    public class EventPartRecord : ContentPartRecord {
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Email { get; set; }
        public virtual string Website { get; set; }

        public virtual string Address { get; set; }
        public virtual string AdditionalAddressInfo { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string ZipCode { get; set; }

        public virtual string GpsLat { get; set; }
        public virtual string GpsLng { get; set; }

        public virtual string ContactName { get; set; }
        public virtual string ContactEmail { get; set; }
        public virtual string ContactPhone { get; set; }
    }
}