﻿using Orchard.ContentManagement;
using Orchard.Localization;
using System.Globalization;
using Orchard.Events;
using Contrib.Events.Models;
using System;
using Orchard;
using Orchard.Localization.Services;

namespace Contrib.Events.Tokens {

    public interface ITokenProvider : IEventHandler {
        void Describe(dynamic context);
        void Evaluate(dynamic context);
    }

    public class EventTokens : ITokenProvider {
        private readonly IDateTimeFormatProvider _dateTimeLocalization;
        private readonly IWorkContextAccessor _workContextAccessor;
        private readonly Lazy<CultureInfo> _cultureInfo;

        public Localizer T { get; set; }

        public EventTokens(
            IDateTimeFormatProvider dateTimeLocalization,
            IWorkContextAccessor workContextAccessor)
        {
            _dateTimeLocalization = dateTimeLocalization;
            _workContextAccessor = workContextAccessor;

            _cultureInfo = new Lazy<CultureInfo>(() => CultureInfo.GetCultureInfo(_workContextAccessor.GetContext().CurrentCulture));

            T = NullLocalizer.Instance;
        }

        public void Describe(dynamic context) {
            context.For("Content").Token("EventPart", T("Event Part"), T("Event Part"));
            context.For("EventPart", T("Event"), T("Tokens for Events"))
                .Token("StartDate", T("StartDate"), T("Event Start Date"))
                .Token("EndDate", T("EndDate"), T("Event End Date. Uses Start Date if End Date isn't specified."))
                .Token("Phone", T("Phone"), T("Event Phone"))
                .Token("Email", T("Email"), T("Event Email"))
                .Token("Website", T("Website"), T("Event Website"))
                .Token("Address", T("Address"), T("Event Address"))
                .Token("AdditionalAddressInfo", T("AdditionalAddressInfo"), T("Event Additional Address Info"))
                .Token("City", T("City"), T("Event City"))
                .Token("State", T("State"), T("Event State"))
                .Token("ZipCode", T("ZipCode"), T("Event ZipCode"))
                .Token("GpsLat", T("GpsLat"), T("Event Latitude"))
                .Token("GpsLng", T("GpsLng"), T("Event Longitude"))
                .Token("ContactName", T("ContactName"), T("Event Contact Name"))
                .Token("ContactEmail", T("ContactEmail"), T("Event Contact Email"))
                .Token("ContactPhone", T("ContactPhone"), T("Event Contact Phone"))
                ;
        }

        public void Evaluate(dynamic context) {
            context.For<IContent>("Content")
                .Token("EventPart", (Func<IContent, object>)(content => {
                    var part = content.As<EventPart>();
                    return part.Title + " - " + part.StartDate.Value.ToShortDateString();
                }))
                .Chain("EventPart", "EventPart", (Func<IContent, object>)(content => content.As<EventPart>()));

            context.For<EventPart>("EventPart")
                .Token("StartDate", (Func<EventPart, object>)(d => d.StartDate.Value.ToString(_dateTimeLocalization.ShortDateFormat, _cultureInfo.Value)))
                .Token("EndDate", (Func<EventPart, object>)(d => (d.EndDate.HasValue? d.EndDate.Value : d.StartDate.Value).ToString(_dateTimeLocalization.ShortDateFormat, _cultureInfo.Value)))
                .Token("Phone", (Func<EventPart, object>)(d => d.Phone))
                .Token("Email", (Func<EventPart, object>)(d => d.Email))
                .Token("Website", (Func<EventPart, object>)(d => d.Website))
                .Token("Address", (Func<EventPart, object>)(d => d.Address))
                .Token("AdditionalAddressInfo", (Func<EventPart, object>)(d => d.AdditionalAddressInfo))
                .Token("City", (Func<EventPart, object>)(d => d.City))
                .Token("State", (Func<EventPart, object>)(d => d.State))
                .Token("ZipCode", (Func<EventPart, object>)(d => d.ZipCode))
                .Token("GpsLat", (Func<EventPart, object>)(d => d.GpsLat))
                .Token("GpsLng", (Func<EventPart, object>)(d => d.GpsLng))
                .Token("ContactName", (Func<EventPart, object>)(d => d.ContactName))
                .Token("ContactEmail", (Func<EventPart, object>)(d => d.ContactEmail))
                .Token("ContactPhone", (Func<EventPart, object>)(d => d.ContactPhone))
                .Chain("StartDate", "Date", (Func<EventPart, object>)(d => d.StartDate.Value))
                .Chain("EndDate", "Date", (Func<EventPart, object>)(d => (d.EndDate.HasValue ? d.EndDate.Value : d.StartDate.Value)))
                ;
        }
    }
}