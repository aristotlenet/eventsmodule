﻿using System.Collections.Generic;
using System.Linq;
using Orchard.Core.Contents;
using Orchard.Environment.Extensions.Models;
using Orchard.Security.Permissions;

namespace Contrib.Events {
    public class EventPermissions : IPermissionProvider {
        public Feature Feature { get; set; }
        public static readonly Permission PublishContent = new Permission { Description = "Publish or unpublish Event for others", Name = "Publish_Event", ImpliedBy = new[] { Permissions.PublishContent } };
        public static readonly Permission PublishOwnContent = new Permission { Description = "Publish or unpublish own Event", Name = "PublishOwn_Event", ImpliedBy = new[] { PublishContent, Permissions.PublishOwnContent } };
        public static readonly Permission EditContent = new Permission { Description = "Edit Event for others", Name = "Edit_Event", ImpliedBy = new[] { PublishContent, Permissions.EditContent } };
        public static readonly Permission EditOwnContent = new Permission { Description = "Edit own Event", Name = "EditOwn_Event", ImpliedBy = new[] { EditContent, PublishOwnContent, Permissions.EditOwnContent } };
        public static readonly Permission DeleteContent = new Permission { Description = "Delete Event for others", Name = "Delete_Event", ImpliedBy = new[] { Permissions.DeleteContent } };
        public static readonly Permission DeleteOwnContent = new Permission { Description = "Delete  own Event", Name = "DeleteOwn_Event", ImpliedBy = new[] { DeleteContent, Permissions.DeleteOwnContent } };
        public static readonly Permission ViewContent = new Permission { Description = "View Event by others", Name = "View_Event", ImpliedBy = new[] { EditContent, Permissions.ViewContent } };
        public static readonly Permission ViewOwnContent = new Permission { Description = "View own Event", Name = "ViewOwn_Event", ImpliedBy = new[] { ViewContent, Permissions.ViewOwnContent } };

        public static readonly Permission MetaListOwnEvents = new Permission { ImpliedBy = new[] { EditOwnContent, DeleteOwnContent, PublishOwnContent } };
        public static readonly Permission MetaListEvents = new Permission { ImpliedBy = new[] { EditContent, DeleteContent, PublishContent } };

        public IEnumerable<Permission> GetPermissions() {
            return Enumerable.Empty<Permission>();
        }

        public IEnumerable<PermissionStereotype> GetDefaultStereotypes() {
            return Enumerable.Empty<PermissionStereotype>();
        }
    }
}
