﻿using Orchard.UI.Resources;

namespace Contrib.Events {
    public class ResourceManifest : IResourceManifestProvider {
        public void BuildManifests(ResourceManifestBuilder builder) {
            var manifest = builder.Add();

            manifest.DefineStyle("Events_Edit").SetUrl("contrib-events-edit.css");
            manifest.DefineStyle("Events_Detail").SetUrl("contrib-events-detail.css");
            manifest.DefineScript("GoogleMaps").SetUrl("http://maps.google.com/maps/api/js?sensor=false");
            manifest.DefineScript("jQueryAutoGeocode").SetUrl("jquery.AutoGeocode.js").SetDependencies("jQuery");
        }
    }
}