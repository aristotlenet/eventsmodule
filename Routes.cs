﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Orchard.Mvc.Routes;


namespace Contrib.Events {
    public class Routes : IRouteProvider {
        public IEnumerable<RouteDescriptor> GetRoutes() {
            return new[] {
                new RouteDescriptor {
                    Route = new Route("Events",
                        new RouteValueDictionary {
                            {"area", "Contrib.Events"},
                            {"Controller", "Event"},
                            {"action", "List"}
                        }, 
                        new RouteValueDictionary(),
                        new RouteValueDictionary{{"area", "Contrib.Events"}},
                        new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Route = new Route("Admin/Events",
                        new RouteValueDictionary {
                            {"area", "Contrib.Events"},
                            {"Controller", "EventAdmin"},
                            {"action", "List"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary{{"area", "Contrib.Events"}},
                        new MvcRouteHandler())
                }
            };
        }

        public void GetRoutes(ICollection<RouteDescriptor> routes) {
            foreach (RouteDescriptor route in GetRoutes()) {
                routes.Add(route);
            }
        }
    }
}