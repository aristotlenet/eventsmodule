﻿using System;
using System.Linq;
using System.Web.Mvc;
using Contrib.Events.Models;
using Orchard.ContentManagement;
using Orchard.DisplayManagement;
using Orchard.Localization;
using Orchard.Logging;
using Orchard.Settings;
using Orchard.Themes;
using Orchard.UI.Navigation;

namespace Contrib.Events.Controllers {
    [Themed]
    public class EventController : Controller {
        private readonly IContentManager _contentManager;
        private readonly ISiteService _siteService;

        public EventController(
            IContentManager contentManager, 
            IShapeFactory shapeFactory,
            ISiteService siteService)
        {
            _contentManager = contentManager;
            _siteService = siteService;
            Logger = NullLogger.Instance;
            Shape = shapeFactory;
            T = NullLocalizer.Instance;
        }

        dynamic Shape { get; set; }
        protected ILogger Logger { get; set; }
        public Localizer T { get; set; }

        public ActionResult List(PagerParameters pagerParameters, int? month) {
            var pager = new Pager(_siteService.GetSiteSettings(), pagerParameters);
            var query = _contentManager.Query<EventPart>(VersionOptions.Published).OrderBy<EventPartRecord>(e => e.StartDate);
            var today = DateTime.Today;

            query = query.Where(e => e.EndDate >= today);

            if (month.HasValue) {
                var filterDate = new DateTime(today.Year, month.Value, today.Day);
                var filterEndDate = filterDate.AddMonths(1).AddDays(2 - filterDate.Day);

                if (filterDate < today) {
                    filterDate = filterDate.AddYears(1);
                }

                if (filterDate.Month != today.Month) {
                    filterDate = filterDate.AddDays(1 - filterDate.Day);
                }

                query = query.Where(e => (e.StartDate.Value.Date <= filterDate && filterDate < e.EndDate.Value.Date) || (e.StartDate.Value.Date <= filterEndDate && filterEndDate < e.EndDate.Value.Date));
            }

            query = query.OrderBy(x => x.StartDate);

            var pageOfEvents = query.Slice(pager.GetStartIndex(), pager.PageSize).ToList();

            var search = _contentManager.New("EventSearch");
            search.Weld(new EventSearchPart());

            var searchShape = _contentManager.BuildDisplay(search);

            var list = Shape.List();
            list.AddRange(pageOfEvents.Select(e => _contentManager.BuildDisplay(e, "Summary")));

            dynamic viewModel = Shape.ViewModel()
                .EventSearch(searchShape.EventSearch)
                .Months(Enumerable.Range(1,12))
                .ContentItems(list)
                .Pager(Shape.Pager(pager).TotalItemCount(query.Count()));

            //searchShape.viewModel = viewModel;

            return View(viewModel);
        }
    }
}