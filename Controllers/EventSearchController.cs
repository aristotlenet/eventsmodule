﻿using System;
using System.Linq;
using System.Web.Mvc;
using Contrib.Events.Models;
using Orchard.ContentManagement;
using Orchard.DisplayManagement;
using Orchard.Environment.Extensions;
using Orchard.Indexing;
using Orchard.Localization;
using Orchard.Logging;
using Orchard.Settings;
using Orchard.Themes;
using Orchard.UI.Navigation;

namespace Contrib.Events.Controllers {
    [Themed]
    [OrchardFeature("Contrib.Events.Search")]
    public class EventSearchController : Controller {
        private readonly IContentManager _contentManager;
        private readonly ISiteService _siteService;
        private readonly IIndexProvider _indexProvider;

        public EventSearchController(
            IContentManager contentManager, 
            IShapeFactory shapeFactory,
            ISiteService siteService,
            IIndexProvider indexProvider)
        {
            _contentManager = contentManager;
            _siteService = siteService;
            _indexProvider = indexProvider;
            Logger = NullLogger.Instance;
            Shape = shapeFactory;
            T = NullLocalizer.Instance;
        }

        dynamic Shape { get; set; }
        protected ILogger Logger { get; set; }
        public Localizer T { get; set; }

        public ActionResult List(PagerParameters pagerParameters, int? month, string keywords, DateTime? startDate, DateTime? endDate) {
            var builder = _indexProvider.CreateSearchBuilder("Event");
            var today = DateTime.Today;

            if (month.HasValue) {
                startDate = new DateTime(today.Year, month.Value, today.Day);
                endDate = startDate.Value.AddMonths(1).AddDays(1 - startDate.Value.Day);

                if (startDate < today) {
                    startDate = startDate.Value.AddYears(1);
                }

                if (startDate.Value.Month != today.Month) {
                    startDate = startDate.Value.AddDays(1 - startDate.Value.Day);
                }
            }

            if (startDate.HasValue) {
                if (startDate.Value < today)
                    startDate = today;

                builder.WithinRange("event_endDate", startDate, null).Mandatory();
            }
            else {
                builder.WithinRange("event_endDate", today, null).Mandatory();
            }

            if (endDate.HasValue) {
                if (endDate.Value < today)
                    endDate = today;

                builder.WithinRange("event_startDate", null, endDate).Mandatory();
            }
            else {
                builder.WithinRange("event_startDate", null, today.AddYears(1)).Mandatory();
            }

            if (!String.IsNullOrEmpty(keywords)) {
                builder.WithField("title", keywords).AsFilter();
                builder.WithField("description", keywords).AsFilter();
            }

            builder.SortByDateTime("event_startDate").Ascending();

            var pager = new Pager(_siteService.GetSiteSettings(), pagerParameters);
            var eventCount = builder.Count();
            var contentItemIds = builder
                .Slice(pager.GetStartIndex(), pager.PageSize)
                .Search()
                .Select(x => x.ContentItemId)
                .ToArray();

            var query = _contentManager.GetMany<EventPart>(contentItemIds, VersionOptions.Published, QueryHints.Empty);

            var search = _contentManager.New("EventSearch");
            search.Weld(new EventSearchPart());

            var searchShape = _contentManager.BuildDisplay(search);
            var list = Shape.List();
            list.AddRange(query.Select(e => _contentManager.BuildDisplay(e, "Summary")));

            var viewModel = Shape.ViewModel()
                .EventSearch(searchShape.EventSearch)
                .Months(Enumerable.Range(1, 12))
                .ContentItems(list)
                .Pager(Shape.Pager(pager).TotalItemCount(eventCount));

            return View("Event/List", viewModel);
        }
    }
}