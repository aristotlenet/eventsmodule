﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Contrib.Events.Models;
using Contrib.Events.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Core.Common.Models;
using Orchard.Core.Contents;
using Orchard.Data;
using Orchard.DisplayManagement;
using Orchard.Localization;
using Orchard.Mvc;
using Orchard.Settings;
using Orchard.UI.Admin;
using Orchard.UI.Navigation;
using Orchard.UI.Notify;

namespace Contrib.Events.Controllers {
    [Admin]
    public class EventAdminController : Controller {
        private readonly ISiteService _siteService;
        private readonly IContentManager _contentManager;
        private readonly ITransactionManager _transactionManager;

        public EventAdminController(
            ISiteService siteService,
            IContentManager contentManager,
            IShapeFactory shapeFactory,
            IOrchardServices services,
            ITransactionManager transactionManager) {
            _siteService = siteService;
            _contentManager = contentManager;
            _transactionManager = transactionManager;
            Services = services;

            T = NullLocalizer.Instance;
            Shape = shapeFactory;
        }

        private dynamic Shape { get; set; }
        public Localizer T { get; set; }
        public IOrchardServices Services { get; set; }

        public ActionResult List(EventAdminContentOptions options, PagerParameters pagerParameters) {
            var pager = new Pager(_siteService.GetSiteSettings(), pagerParameters);
            var query = _contentManager.Query(VersionOptions.Latest, "Event");

            switch (options.Filter) {
                case EventsStatus.Active:
                    query = query.ForVersion(VersionOptions.Published);
                    break;
                case EventsStatus.Archived:
                    query = query
                        .ForVersion(VersionOptions.Draft)
                        .Where<EventPartRecord>(e => e.EndDate <= DateTime.Today);
                    break;
                case EventsStatus.Pending:
                    query = query
                        .ForVersion(VersionOptions.Draft)
                        .Where<EventPartRecord>(e => e.EndDate >= DateTime.Today);
                    break;
                case EventsStatus.All:
                    break;
            }

            switch (options.OrderBy) {
                case EventsOrder.StartDate:
                    query = query.OrderByDescending<EventPartRecord>(cr => cr.StartDate);
                    break;
                case EventsOrder.EndDate:
                    query = query.OrderByDescending<EventPartRecord>(cr => cr.EndDate);
                    break;
                case EventsOrder.Modified:
                    query = query.OrderByDescending<CommonPartRecord>(cr => cr.ModifiedUtc);
                    break;
                case EventsOrder.Published:
                    query = query.OrderByDescending<CommonPartRecord>(cr => cr.PublishedUtc);
                    break;
                case EventsOrder.Created:
                    query = query.OrderByDescending<CommonPartRecord>(cr => cr.CreatedUtc);
                    break;
            }

            if (Services.Authorizer.Authorize(EventPermissions.MetaListOwnEvents) && !Services.Authorizer.Authorize(EventPermissions.MetaListEvents)) {
                var ownerId = Services.WorkContext.CurrentUser.Id;
                query = query.Where<CommonPartRecord>(cr => cr.OwnerId == ownerId);
            }

            var itemCount = query.Count();
            var pageOfEvents = query.Slice(pager.GetStartIndex(), pager.PageSize).ToList();

            var list = Shape.List();
            list.AddRange(pageOfEvents.Select(e => _contentManager.BuildDisplay(e, "SummaryAdmin")));


            dynamic viewModel = Shape.ViewModel()
                .ContentItems(list)
                .Pager(Shape.Pager(pager).TotalItemCount(itemCount))
                .Options(options)
                .TypeDisplayName("Events");

            return View(viewModel);
        }

        [HttpPost, ActionName("List")]
        [FormValueRequired("submit.Filter")]
        public ActionResult ListFilterPost(EventAdminContentOptions options) {
            var routeValues = ControllerContext.RouteData.Values;
            if (options != null) {
                routeValues["Options.OrderBy"] = options.OrderBy;
                routeValues["Options.Filter"] = options.Filter;
            }

            return RedirectToAction("List", routeValues);
        }


        [HttpPost, ActionName("List")]
        [FormValueRequired("submit.BulkAction")]
        public ActionResult ListPost(EventAdminContentOptions options, IEnumerable<int> itemIds) {
            if (itemIds != null) {
                var checkedEntries = _contentManager.GetMany<ContentItem>(itemIds, VersionOptions.Latest, QueryHints.Empty);

                switch (options.BulkAction) {
                    case EventsBulkAction.None:
                        break;
                    case EventsBulkAction.Publish:
                        foreach (var entry in checkedEntries) {
                            if (!Services.Authorizer.Authorize(Permissions.PublishContent, entry, T("Couldn't publish events"))) {
                                _transactionManager.Cancel();
                                return new HttpUnauthorizedResult();
                            }

                            _contentManager.Publish(entry);
                        }

                        Services.Notifier.Information(T("Events successfully published."));
                        break;
                    case EventsBulkAction.Unpublish:
                        foreach (var entry in checkedEntries)
                        {
                            if (!Services.Authorizer.Authorize(Permissions.PublishContent, entry, T("Couldn't unpublish selected content.")))
                            {
                                _transactionManager.Cancel();
                                return new HttpUnauthorizedResult();
                            }

                            _contentManager.Unpublish(entry);
                        }
                        Services.Notifier.Information(T("Content successfully unpublished."));
                        break;
                    case EventsBulkAction.Delete:
                        foreach (var entry in checkedEntries) {
                            if (!Services.Authorizer.Authorize(Permissions.DeleteContent, entry, T("Couldn't delete events"))) {
                                _transactionManager.Cancel();
                                return new HttpUnauthorizedResult();
                            }

                            _contentManager.Remove(entry);
                        }

                        Services.Notifier.Information(T("Events successfully deleted."));
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return RedirectToAction("List");
        }
    }
}