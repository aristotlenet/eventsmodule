﻿Name: Events
AntiForgery: enabled
Author: Aristotle
Website: http://www.aristotle.net
Version: 1.3.6
OrchardVersion: 1.8
Description: Implements basic events features
Features:
    Contrib.Events:
        Category: Events
        Dependencies: Orchard.Fields, Feeds, Orchard.Widgets, Orchard.PublishLater, Orchard.ArchiveLater, Orchard.Taxonomies
    Contrib.Events.Search:
        Name: Event Search
        Category: Search
        Dependencies: Contrib.Events, Orchard.Search
