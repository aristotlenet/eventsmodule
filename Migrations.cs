﻿using System.Linq;
using Contrib.Events.Models;
using Orchard.ArchiveLater.Models;
using Orchard.ContentManagement;
using Orchard.Data.Migration;
using Orchard.ContentManagement.MetaData;
using Orchard.Core.Contents.Extensions;
using System;
using Orchard.Environment.Extensions;
using Orchard.Indexing;
using Orchard.Tasks.Scheduling;
using Orchard.Taxonomies.Models;


namespace Contrib.Events
{
    public class Migrations : DataMigrationImpl {
        private readonly IContentManager _contentManager;
        private readonly IScheduledTaskManager _scheduledTaskManager;


        public Migrations(IContentManager contentManager,
                IScheduledTaskManager scheduledTaskManager) {
            _contentManager = contentManager;
            _scheduledTaskManager = scheduledTaskManager;
        }

        public int Create(){
            SchemaBuilder.CreateTable(typeof(EventPartRecord).Name,
                table => table
                    .ContentPartRecord()
                    .Column<DateTime>("StartDate")
                    .Column<DateTime>("EndDate", column => column.Nullable())
                    .Column<string>("Phone")
                    .Column<string>("Email")
                    .Column<string>("Website")
                    .Column<string>("Address")
                    .Column<string>("AdditionalAddressInfo")
                    .Column<string>("City")
                    .Column<string>("State")
                    .Column<string>("ZipCode")
                    .Column<string>("GpsLat")
                    .Column<string>("GpsLng")
                    .Column<string>("ContactName")
                    .Column<string>("ContactEmail")
                    .Column<string>("ContactPhone")
            );

            ContentDefinitionManager.AlterPartDefinition(typeof(EventPart).Name, builder => builder
                .WithDescription("Turns content types into Events"));

            var taxonomyContentItem = _contentManager.New("Taxonomy");
            var taxonomyPart = taxonomyContentItem.As<TaxonomyPart>();

            taxonomyPart.Name = "Event Type";
            taxonomyPart.IsInternal = true;

            _contentManager.Create(taxonomyContentItem, VersionOptions.Draft);
            _contentManager.Publish(taxonomyContentItem);

            ContentDefinitionManager.AlterPartDefinition("Event",
                cfg => cfg
                    .Attachable()
                    .WithField("EventType", 
                        f => f
                        .OfType("TaxonomyField")
                        .WithDisplayName("Event Type")
                        .WithSetting("TaxonomyFieldSettings.Taxonomy", taxonomyPart.Name))
            );

            ContentDefinitionManager.AlterTypeDefinition("Event",
                cfg => cfg
                    .WithPart("Event")
                    .WithPart("EventPart")
                    .WithPart("CommonPart")
                    .WithPart("TitlePart")
                    .WithPart("BodyPart")
                    .WithPart("AutoroutePart", builder => builder
                        .WithSetting("AutorouteSettings.AllowCustomPattern", "true")
                        .WithSetting("AutorouteSettings.AutomaticAdjustmentOnEdit", "false")
                        .WithSetting("AutorouteSettings.PatternDefinitions", "[{Name: 'Title', Pattern: 'events/{Content.EventPart.StartDate.Format:yyyy/MM/dd}/{Content.Slug}', Description: 'events/year/month/day/my-event'}]")
                        .WithSetting("AutorouteSettings.DefaultPatternIndex", "0"))
                    .WithPart("PublishLaterPart")
                    .WithPart("ArchiveLaterPart")
                    .WithPart("IdentityPart")
                    .Draftable()
                );

            return 1;
        }

        public int UpdateFrom1() {
            ContentDefinitionManager.AlterTypeDefinition("Event",
                cfg => cfg.Creatable());
            return 2;
        }

        public int UpdateFrom2() {
            var query = _contentManager.List<EventPart>()
                .Where(x => !x.As<ArchiveLaterPart>().ScheduledArchiveUtc.Value.HasValue)
                .Where(x => x.ContentItem.IsPublished());
            var today = DateTime.Now;
            const string unpublishTaskType = "Unpublish";

            foreach (var eventPart in query) {
                if (eventPart.EndDate < today) {
                    _contentManager.Unpublish(eventPart.ContentItem);
                }
                else {
                    _scheduledTaskManager.CreateTask(unpublishTaskType, eventPart.EndDate.Value, eventPart.ContentItem);
                }
            }

            return 3;
        }
    }

    [OrchardFeature("Contrib.Events.Search")]
    public class EventSearchMigrations : DataMigrationImpl {
        private readonly IIndexManager _indexManager;

        public EventSearchMigrations(IIndexManager indexManager) {
            _indexManager = indexManager;
        }

        public int Create() {
            _indexManager.GetSearchIndexProvider().CreateIndex("Event");

            ContentDefinitionManager.AlterTypeDefinition("Event", cfg => cfg.WithSetting("TypeIndexing.Indexes", "Event"));

            return 1;
        }
    }
}
