﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Orchard.Environment.Extensions;
using Orchard.Mvc.Routes;

namespace Contrib.Events {
    [OrchardFeature("Contrib.Events.Search")]
    public class EventSearchRoutes : IRouteProvider {
        public IEnumerable<RouteDescriptor> GetRoutes() {
            return new[] {
                new RouteDescriptor {
                    Priority=10,
                    Route = new Route("Events",
                        new RouteValueDictionary {
                            {"area", "Contrib.Events"},
                            {"Controller", "EventSearch"},
                            {"action", "List"}
                        }, 
                        new RouteValueDictionary(),
                        new RouteValueDictionary{{"area", "Contrib.Events"}},
                        new MvcRouteHandler())
                }
            };
        }

        public void GetRoutes(ICollection<RouteDescriptor> routes) {
            foreach (RouteDescriptor route in GetRoutes()) {
                routes.Add(route);
            }
        }
    }
}